/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef UDPRECEIVER_H_
#define UDPRECEIVER_H_

#include <cstring>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <bitset>

#include "NetworkConstants.h"

/**
 * @brief      Wrapper class that holds a UDP socket for receiving messages.
 */
class UDPReceiver
{
public:

    /**
     * @brief      Constructs a UDPReceiver object
     *
     * @param[in]  receivePort  The receive port
     */
    UDPReceiver(unsigned int receivePort);
    ~UDPReceiver();
    
    /**
     * @brief      Receives a single UDP message
     *
     * @return     True if UDP message is received, false otherwise
     */
    bool ReceiveMessage();

    bool setMulticastIPAddress(struct in_addr multicastAddress);

    /**
     * @brief      Gets a uint8_t pointer received message.
     *
     * @return     Pointer to the received message.
     */
    inline uint8_t* getReceivedMessage()
    {
        return &receivedMessage_[0];
    }

    /**
     * @brief      Gets the received number of bytes.
     *
     * @return     The received number of bytes.
     */
    inline int getReceivedNumOfBytes()
    {
        return receivedNumOfBytes_;
    }

    struct sockaddr_in thisAddress_;/** < Holds address of the Jupiter sensor
                                     */
    struct sockaddr_in remoteAddress_;/**< Holds address of machine from which UDP messages are coming, set to empty*/
    std::string ipAddressStr_;
private:
    static constexpr unsigned int bufferSize_ = NetworkConstants::maximumNumOfBytesInUDPMessage;
    unsigned int receivePort_;/** < Holds the receiving UDP socket */
    int socketEndpoint_;/** < Holds the file descriptor of receiving UDP socket */
    socklen_t socketLength_;/** < Holds size of UDP socket */
    uint8_t receivedMessage_[bufferSize_]; /** < Holds received message as a byte array */
    int receivedNumOfBytes_;/** < Holds number of bytes in received UDP message */
    bool debugMode_;/** < Turns on/off debug console messages */

};
#endif