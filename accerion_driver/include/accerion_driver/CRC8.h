/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef CRC8_H
#define CRC8_H
#include <stdint.h>
#include <iostream>
typedef uint8_t crc;
#define WIDTH  (8 * sizeof(crc))
#define POLYNOMIAL 0xD8  /* 11011 followed by 0's */
#define TOPBIT (1 << (WIDTH - 1))
extern crc crcTable[256];

class CRC8
{
public:
	CRC8();
	~CRC8();
	void crcInit();
	crc crcFast(uint8_t const message[], int nBytes);
};

#endif