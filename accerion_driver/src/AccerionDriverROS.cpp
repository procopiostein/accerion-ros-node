/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDriverROS.h"

AccerionDriverROS::AccerionDriverROS()
{
    pnh_ = new ros::NodeHandle("~");

    nodeNs_ = ros::this_node::getName();
    ROS_INFO("nodeNs_: %s", nodeNs_.c_str());
    // read ros params
    absRefFrame_         = pnh_->param <std::string>("abs_ref_frame",       "map");

    jupiterSerial_       = pnh_->param( "serial_number", 0);
    relOdomOn_           = pnh_->param( "ros_publishing/rel_odom",        true);
    relTransOn_          = pnh_->param( "ros_publishing/rel_tf",          true);
    absOdomOn_           = pnh_->param( "ros_publishing/abs_odom",        true);  
    absTransOn_          = pnh_->param( "ros_publishing/abs_tf",          true);
    relInAbsTransOn_     = pnh_->param( "ros_publishing/rel_in_abs_tf",   true);
    absBaseFrame_        = pnh_->param <std::string>("abs_base_frame",   "sensor_output_abs");
    relRefFrame_         = pnh_->param <std::string>("rel_ref_frame",    "odom");
    relBaseFrame_        = pnh_->param <std::string>("rel_base_frame",   "sensor_output_rel");
    relSensorFrame_      = pnh_->param <std::string>("rel_sensor_frame", "sensor_mount_rel");
    absSensorFrame_      = pnh_->param <std::string>("abs_sensor_frame", "sensor_mount_abs");
    debugModeStreaming_  = pnh_->param("debug/debug_sensor_messages", false);
    extRefPoseTopic_     = pnh_->param <std::string>("debug/external_reference_pose_topic", "ext_ref");
    extRefPoseLatency_   = pnh_->param("debug/external_reference_pose_latency", 0.0);
    
    if(pnh_->param("debug/use_external_reference_pose", false))
    {
        subsExternalOdometry_       = nh_.subscribe(extRefPoseTopic_, 1, &AccerionDriverROS::callbackExternalReferencePose, this, ros::TransportHints().tcpNoDelay());
        ROS_INFO("NOTE: External odometry input %s will be used to set Jupiter sensor pose continuously!!! EXPERIMENTAL", extRefPoseTopic_.c_str());
    }

    // initialize publishers and subscribers
    if(relOdomOn_)      
        pubRelOdom_  = nh_.advertise<nav_msgs::Odometry>("rel_odom", 1);
    if(absOdomOn_)      
        pubAbsOdom_  = nh_.advertise<nav_msgs::Odometry>("abs_map", 1);
    
    pubDriftDetails_       = nh_.advertise<accerion_driver_msgs::DriftCorrectionDetails>("correction_details", 1);
    pubDiagnostics_        = nh_.advertise<accerion_driver_msgs::SensorDiagnostics>("diagnostics", 1);
    pubLineFollower_       = nh_.advertise<geometry_msgs::PolygonStamped>("line_follower", 1);
    pubAbsCorrections_     = nh_.advertise<nav_msgs::Odometry>("corrections", 1);
    pubMapAsPC2_           = nh_.advertise <sensor_msgs::PointCloud2> ("map", 10, true);
    pubArucoVisualization_ = nh_.advertise <visualization_msgs::MarkerArray> ("aruco_in_sensor_frame", 5, false);

    subsResetPose_     = nh_.subscribe("reset_pose_manual",        1, &AccerionDriverROS::callbackResetPose, this);
 
    serviceServerRecoveryMode_        = nh_.advertiseService("mode/set_recovery_search_radius", &AccerionDriverROS::callbackServiceRecoveryMode, this);
    serviceServerDeleteCluster_       = nh_.advertiseService("delete_cluster", &AccerionDriverROS::callbackServiceDeleteCluster, this);
    serviceServerDeleteAllClusters_   = nh_.advertiseService("delete_all_clusters", &AccerionDriverROS::callbackServiceDeleteAllClusters, this);
    srvServerMode_                    = nh_.advertiseService("mode", &AccerionDriverROS::callbackServiceMode, this);
    srvServerModeWithCluster_         = nh_.advertiseService("mode_with_cluster", &AccerionDriverROS::callbackServiceModeWithCluster, this);
    srvServerRequest_                 = nh_.advertiseService("request", &AccerionDriverROS::callbackServiceRequest, this);
    serviceServerResetPoseByTfLookup_ = nh_.advertiseService("reset_pose_by_tf_lookup", &AccerionDriverROS::callbackServiceResetPoseByTfLookup, this);
    serviceServerSetPose_             = nh_.advertiseService("set_pose", &AccerionDriverROS::callbackServiceSetPose, this);
    serviceServerSetUDPSettings_      = nh_.advertiseService("set_udp_settings", &AccerionDriverROS::callbackServiceSetUDPSettings, this);

    //init udp manager
    udpManager_     = new UDPManager(jupiterSerial_);
  
    //initialize other variables
    correctionCounter_      = 0;
    jupiIp_                 = "0";
    infoCount_              = 0;
    noInfoCount_            = 0;
    sensorConnected_        = false;

    mapPointCloud_.header.frame_id  = absRefFrame_;
    mapPointCloud_.is_bigendian     = false;
    mapPointCloud_.is_dense         = true;

    mapPointCloud_.width  = 0;
    mapPointCloud_.height = 1;
    mapPointCloud_.fields.resize(3);

    // Set x/y/z as the only fields
    mapPointCloud_.fields[0].name = "x";
    mapPointCloud_.fields[1].name = "y";
    mapPointCloud_.fields[2].name = "z";

    int offset = 0;
    // All offsets are *4, as all field data types are float32
    for (size_t d = 0; d < mapPointCloud_.fields.size(); ++d, offset += 4)
    {
        mapPointCloud_.fields[d].count    = 1;
        mapPointCloud_.fields[d].offset   = offset;
        mapPointCloud_.fields[d].datatype = sensor_msgs::PointField::FLOAT32;
    }

    mapPointCloud_.point_step = offset;
    mapPointCloud_.row_step = mapPointCloud_.point_step * mapPointCloud_.width;

    mapPointCloud_.data.resize(mapPointCloud_.width * mapPointCloud_.point_step);
   
    pc2Modifier_ = new sensor_msgs::PointCloud2Modifier(mapPointCloud_);
    
    pc2Modifier_->setPointCloud2Fields(6,   "x",            1, sensor_msgs::PointField::FLOAT32,
                                            "y",            1, sensor_msgs::PointField::FLOAT32,
                                            "z",            1, sensor_msgs::PointField::FLOAT32,
                                            "markerID",     1, sensor_msgs::PointField::UINT32,
                                            "clusterID",    1, sensor_msgs::PointField::UINT16,
                                            "markerStatus", 1, sensor_msgs::PointField::UINT8);

}

int AccerionDriverROS::run()
{
    double x, y, th;
    geometry_msgs::Quaternion q;

    infoCount_ = 0;
    while(udpManager_->receiveMessage())
    {
        uint8_t*     receivedMessage      = udpManager_->getReceivedMessage();
        unsigned int receivedMessageSize  = udpManager_->getReceivedNumberOfBytes();
        uint8_t      receivedCommandID    = udpManager_->getReceivedCommandID();
        uint32_t     receivedSerialNumber = udpManager_->getReceivedSerialNumber();

        if(debugModeStreaming_) 
        {
            std::string udpBinaryMessageInString    = udpManager_->getUDPBinaryMessageInString();
            ROS_INFO("rcv udp: %s", (std::get<0>(Commands::commandValues.at(receivedCommandID)).c_str()));
            ROS_INFO("rcv udp: SN = %9d  cmdID = 0x%2x := %2d  msgSize = %2d", receivedSerialNumber, receivedCommandID, (unsigned)receivedCommandID, receivedMessageSize);
            ROS_INFO("rcv udp: msg :\n%s", udpBinaryMessageInString.c_str());
        }

        if(receivedSerialNumber == jupiterSerial_)
        {
            noInfoCount_ = 0;
            infoCount_++;

            if(!sensorConnected_)
            {
                ROS_INFO("%s: received data from Jupiter with SN: %d with IP: %s, publishing happily", nodeNs_.c_str(), receivedSerialNumber, jupiIp_.c_str());
                sensorConnected_ = true;
            }

            ros::Time rosTimeStamp = ros::Time::now();

            switch(receivedCommandID)
            {
                // heartbeat message
                case Commands::PRD_HEARTBEAT_INFO :
                {
                    char deviceIp[20], unicastIp[20];
                    inet_ntop(AF_INET, &receivedMessage[5], deviceIp, 20);
                    inet_ntop(AF_INET, &receivedMessage[9], unicastIp, 20);
                    // store the ip from Jupiter
                    if(jupiIp_ == "0")
                    {
                        jupiIp_ = deviceIp;
                        ROS_INFO("Received first PRD_HEARTBEAT_INFO with sensor IP address: %s with UDP unicast IP address: %s", jupiIp_.c_str(), unicastIp);

                        udpManager_->setSerialNumber(jupiterSerial_);
                        udpManager_->setReceiver(jupiIp_);
                        udpManager_->setJupiterTimeAndDate();
                        std::vector<uint8_t> dataToTransmit;
                        udpManager_->sendUDPMessageToJupiter(Commands::CMD_GET_SENSOR_MOUNT_POSE, dataToTransmit);
                        udpManager_->sendUDPMessageToJupiter(Commands::CMD_GET_ALL_ACKNOWLEDGEMENTS, dataToTransmit);
                    }
                    else if(jupiIp_ != deviceIp)
                    {
                        ROS_WARN("Received a PRD_HEARTBEAT_INFO from sensor %d with a different IP address: %s", jupiterSerial_, deviceIp);
                        jupiIp_ = deviceIp;
                        ROS_WARN("Changing target ip address to %s", jupiIp_.c_str());
                        udpManager_->setReceiver(jupiIp_);
                    }
                    break;
                }
                // odometry related
                case Commands::STR_CORRECTED_POSE_DATA_LIGHT :
                {
                    rawTimestampAbs_ = utils::getTimestampInSecs(&receivedMessage[5]);

                    if(absOdomOn_)
                    {
                        nav_msgs::Odometry absPoseMsg;
                        absPoseMsg.header.stamp              = rosTimeStamp;
                        absPoseMsg.header.frame_id           = absRefFrame_;
                        absPoseMsg.child_frame_id            = absBaseFrame_;
                        absPoseMsg.pose.pose.position.x      = utils::getMeter(&receivedMessage[13]);
                        absPoseMsg.pose.pose.position.y      = utils::getMeter(&receivedMessage[17]);
                        absPoseMsg.pose.pose.position.z      = 0.0;
                        double theta = utils::getRad32(&receivedMessage[21]);
                        absPoseMsg.pose.pose.orientation.x   = 0.0;
                        absPoseMsg.pose.pose.orientation.y   = 0.0;
                        absPoseMsg.pose.pose.orientation.z   = sin(theta / 2.0);
                        absPoseMsg.pose.pose.orientation.w   = cos(theta / 2.0);
                        absPoseMsg.twist.twist.linear.x      = utils::getMeter(&receivedMessage[25]);
                        absPoseMsg.twist.twist.linear.y      = utils::getMeter(&receivedMessage[29]);
                        absPoseMsg.twist.twist.angular.z     = utils::getRad16(&receivedMessage[33]);

                        uint64_t poseStdDevX  = (uint64_t) utils::getUInt32(&receivedMessage[35]);
                        uint64_t poseStdDevY  = (uint64_t) utils::getUInt32(&receivedMessage[39]);
                        uint64_t poseStdDevTh = (uint64_t) utils::getUInt32(&receivedMessage[43]);
                        uint64_t velStdDevX   = (uint64_t) utils::getUInt32(&receivedMessage[47]);
                        uint64_t velStdDevY   = (uint64_t) utils::getUInt32(&receivedMessage[51]);
                        uint64_t velStdDevTh  = (uint64_t) utils::getUInt32(&receivedMessage[55]);

                        absPoseMsg.pose.covariance[0]   = (poseStdDevX*poseStdDevX)/1000000000000.0;
                        absPoseMsg.pose.covariance[7]   = (poseStdDevY*poseStdDevY)/1000000000000.0;
                        absPoseMsg.pose.covariance[35]  = (poseStdDevTh*poseStdDevTh)*(M_PI*M_PI)/(180.0*180.0*10000.0);

                        absPoseMsg.twist.covariance[0]  = velStdDevX*velStdDevX/1000000000000.0;
                        absPoseMsg.twist.covariance[7]  = velStdDevY*velStdDevY/1000000000000.0;
                        absPoseMsg.twist.covariance[35] = (velStdDevTh*velStdDevTh)*(M_PI*M_PI)/(180.0*180.0*10000.0);
                        pubAbsOdom_.publish(absPoseMsg);
                    }

                    absPoseTrans_.header.stamp            = rosTimeStamp;
                    absPoseTrans_.header.frame_id         = absRefFrame_;
                    absPoseTrans_.child_frame_id          = absBaseFrame_;
                    absPoseTrans_.transform.translation.x = utils::getMeter(&receivedMessage[13]);
                    absPoseTrans_.transform.translation.y = utils::getMeter(&receivedMessage[17]);
                    absPoseTrans_.transform.translation.z = 0.0;
                    double theta = utils::getRad32(&receivedMessage[21]);
                    absPoseTrans_.transform.rotation.x    = 0.0;
                    absPoseTrans_.transform.rotation.y    = 0.0;
                    absPoseTrans_.transform.rotation.z    = sin(theta / 2.0);
                    absPoseTrans_.transform.rotation.w    = cos(theta / 2.0);
                
                    if(absTransOn_)
                    {
                        tfBroadcaster_.sendTransform(absPoseTrans_);
                    }

                    if(relInAbsTransOn_ && rawTimestampAbs_ == rawTimestampRel_)
                    {
                        double xtmp, ytmp, thtmp;
                        xtmp = -relOdomTrans_.transform.translation.x;
                        ytmp = -relOdomTrans_.transform.translation.y;
                        thtmp = getThetaFromQuat(relOdomTrans_.transform.rotation);

                        utils::rotate2d(xtmp, ytmp, -thtmp);
                        utils::rotate2d(xtmp, ytmp, theta);

                        double relInAbsTh = utils::getAngularDiff(theta, thtmp);

                        geometry_msgs::TransformStamped relInAbsTrans;
                        relInAbsTrans.header.stamp            = rosTimeStamp;
                        relInAbsTrans.header.frame_id         = absRefFrame_;
                        relInAbsTrans.child_frame_id          = relRefFrame_;
                        relInAbsTrans.transform.translation.x = utils::getMeter(&receivedMessage[13]) + xtmp;
                        relInAbsTrans.transform.translation.y = utils::getMeter(&receivedMessage[17]) + ytmp;
                        relInAbsTrans.transform.translation.z = 0.0;
                        relInAbsTrans.transform.rotation.x    = 0.0;
                        relInAbsTrans.transform.rotation.y    = 0.0;
                        relInAbsTrans.transform.rotation.z    = sin(relInAbsTh / 2.0);
                        relInAbsTrans.transform.rotation.w    = cos(relInAbsTh / 2.0);

                        tfBroadcaster_.sendTransform(relInAbsTrans);
                    } 

                    break;
                }
                case Commands::STR_UNCORRECTED_POSE_DATA :
                {      
                    rawTimestampRel_ = utils::getTimestampInSecs(&receivedMessage[5]);                   
                    if(relOdomOn_)
                    {
                        nav_msgs::Odometry relOdomMsg;
                        relOdomMsg.header.stamp              = rosTimeStamp;
                        relOdomMsg.header.frame_id           = relRefFrame_;
                        relOdomMsg.child_frame_id            = relBaseFrame_;

                        relOdomMsg.pose.pose.position.x      = utils::getMeter(&receivedMessage[13]);
                        relOdomMsg.pose.pose.position.y      = utils::getMeter(&receivedMessage[17]);
                        relOdomMsg.pose.pose.position.z      = 0.0;
                
                        double theta = utils::getRad32(&receivedMessage[21]);
                        relOdomMsg.pose.pose.orientation.x   = 0.0;
                        relOdomMsg.pose.pose.orientation.y   = 0.0;
                        relOdomMsg.pose.pose.orientation.z   = sin(theta / 2.0);
                        relOdomMsg.pose.pose.orientation.w   = cos(theta / 2.0);

                        relOdomMsg.twist.twist.linear.x      = utils::getMeter(&receivedMessage[25]);
                        relOdomMsg.twist.twist.linear.y      = utils::getMeter(&receivedMessage[29]);
                        relOdomMsg.twist.twist.angular.z     = utils::getRad16(&receivedMessage[33]);

                        uint64_t velStdDevX  = (uint64_t) utils::getUInt32(&receivedMessage[35]);
                        uint64_t velStdDevY  = (uint64_t) utils::getUInt32(&receivedMessage[39]);
                        uint64_t velStdDevTh = (uint64_t) utils::getUInt32(&receivedMessage[43]);
                        
                        relOdomMsg.twist.covariance[0]  = (velStdDevX*velStdDevX)/1000000000000.0;
                        relOdomMsg.twist.covariance[7]  = (velStdDevY*velStdDevY)/1000000000000.0;
                        relOdomMsg.twist.covariance[35] = (velStdDevTh*velStdDevTh*M_PI*M_PI)/(180.0*180.0*10000.0);
                        pubRelOdom_.publish(relOdomMsg);
                    }

                    double theta = utils::getRad32(&receivedMessage[21]);
                    relOdomTrans_.header.stamp            = rosTimeStamp;
                    relOdomTrans_.header.frame_id         = relRefFrame_;
                    relOdomTrans_.child_frame_id          = relBaseFrame_;
                    relOdomTrans_.transform.translation.x = utils::getMeter(&receivedMessage[13]);
                    relOdomTrans_.transform.translation.y = utils::getMeter(&receivedMessage[17]);
                    relOdomTrans_.transform.translation.z = 0.0;
                    relOdomTrans_.transform.rotation.x    = 0.0;
                    relOdomTrans_.transform.rotation.y    = 0.0;
                    relOdomTrans_.transform.rotation.z    = sin(theta / 2.0);
                    relOdomTrans_.transform.rotation.w    = cos(theta / 2.0);

                    if(relTransOn_)
                    {
                        tfBroadcaster_.sendTransform(relOdomTrans_);
                    }

                    if(relInAbsTransOn_ && rawTimestampRel_ == rawTimestampAbs_)
                    {
                        double xtmp, ytmp, thtmp;
                        xtmp = -relOdomTrans_.transform.translation.x;
                        ytmp = -relOdomTrans_.transform.translation.y;
                        thtmp = getThetaFromQuat(relOdomTrans_.transform.rotation);

                        double theta_abs = asin(absPoseTrans_.transform.rotation.z)*2.0;

                        utils::rotate2d(xtmp, ytmp, -thtmp);
                        utils::rotate2d(xtmp, ytmp, theta_abs);

                        double relInAbsTh = utils::getAngularDiff(theta_abs, thtmp);

                        geometry_msgs::TransformStamped relInAbsTrans;
                        relInAbsTrans.header.stamp            = rosTimeStamp;
                        relInAbsTrans.header.frame_id         = absRefFrame_;
                        relInAbsTrans.child_frame_id          = relRefFrame_;
                        relInAbsTrans.transform.translation.x = absPoseTrans_.transform.translation.x + xtmp;
                        relInAbsTrans.transform.translation.y = absPoseTrans_.transform.translation.y + ytmp;
                        relInAbsTrans.transform.translation.z = 0.0;
                        relInAbsTrans.transform.rotation.x    = 0.0;
                        relInAbsTrans.transform.rotation.y    = 0.0;
                        relInAbsTrans.transform.rotation.z    = sin(relInAbsTh / 2.0);
                        relInAbsTrans.transform.rotation.w    = cos(relInAbsTh / 2.0);

                        tfBroadcaster_.sendTransform(relInAbsTrans);
                    }  

                    break;
                }
                case Commands::STR_DIAGNOSTICS:
                {
                    accerion_driver_msgs::SensorDiagnostics diagnosticsMsg;

                    diagnosticsMsg.header.stamp    = rosTimeStamp;
                    diagnosticsMsg.serial_number   = receivedSerialNumber;
                    diagnosticsMsg.raw_timestamp   = utils::getUInt64(&receivedMessage[5]);

                    diagnosticsMsg.modes_code    = utils::getUInt16(&receivedMessage[13]);
                    std::bitset<16> modes_bitset(diagnosticsMsg.modes_code);

                    diagnosticsMsg.warnings_code = utils::getUInt16(&receivedMessage[15]);
                    std::bitset<16> warnings_bitset(diagnosticsMsg.warnings_code);

                    diagnosticsMsg.errors_code   = utils::getUInt32(&receivedMessage[17]);
                    std::bitset<32> errors_bitset(diagnosticsMsg.errors_code);

                    diagnosticsMsg.status_code   = receivedMessage[21];
                    std::bitset<8> status_bitset(diagnosticsMsg.status_code);

                    for(unsigned int ii = 0; ii < 32; ii++)
                    {
                        if(ii < 8)
                        {
                            if(status_bitset.test(ii))
                                diagnosticsMsg.active_status += Commands::statusDescriptionMap.find(ii)->second + ",";
                        }

                        if(ii < 16)
                        {
                            if(modes_bitset.test(ii))
                                diagnosticsMsg.active_modes += Commands::modesDescriptionMap.find(ii)->second + ",";
                            
                            if(warnings_bitset.test(ii))
                                diagnosticsMsg.active_warnings += Commands::warningsDescriptionMap.find(ii)->second + ",";
                        }
                        
                        if(errors_bitset.test(ii))
                            diagnosticsMsg.active_errors += Commands::errorsDescriptionMap.find(ii)->second + ",";
                    }

                    pubDiagnostics_.publish(diagnosticsMsg);
                    
                    break;
                }
                case Commands::INT_DRIFT_CORRECTION_DONE:  //drift done
                {
                    nav_msgs::Odometry correctionsMsg;
                    correctionsMsg.header.stamp = rosTimeStamp;

                    correctionsMsg.header.frame_id  = absRefFrame_;
                    correctionsMsg.child_frame_id   = absBaseFrame_;

                    x  = utils::getMeter(&receivedMessage[13]);
                    y  = utils::getMeter(&receivedMessage[17]);
                    th = utils::getRad32(&receivedMessage[21]);

                    correctionsMsg.header.seq           = correctionCounter_;
                    correctionsMsg.pose.pose.position.x = x;
                    correctionsMsg.pose.pose.position.y = y;
                    correctionsMsg.pose.pose.position.z = 0.0;

                    correctionsMsg.pose.pose.orientation.x = 0.0;
                    correctionsMsg.pose.pose.orientation.y = 0.0;
                    correctionsMsg.pose.pose.orientation.z = sin(th / 2.0);
                    correctionsMsg.pose.pose.orientation.w = cos(th / 2.0);

                    /*Note: For now, covariance values in this ROS message are dummy,
                     *since covariance values are not included with the Drift Correction UDP message*/
                    correctionsMsg.pose.covariance[0]  = 0.0;
                    correctionsMsg.pose.covariance[7]  = 0.0;
                    correctionsMsg.pose.covariance[35] = 0.0;

                    correctionCounter_++;

                    pubAbsCorrections_.publish(correctionsMsg);

                    if(receivedMessageSize == 54)
                    {
                        accerion_driver_msgs::DriftCorrectionDetails  driftDetailsMsg;

                        driftDetailsMsg.new_corrected_pose = correctionsMsg.pose;
    
                        driftDetailsMsg.correction_x           = utils::getMeter(&receivedMessage[25]);
                        driftDetailsMsg.correction_y           = utils::getMeter(&receivedMessage[29]);
                        driftDetailsMsg.correction_heading     = utils::getRad32(&receivedMessage[33]);
                        driftDetailsMsg.error_heading_deg      = utils::rad2deg(driftDetailsMsg.correction_heading);
                        driftDetailsMsg.correct_heading_deg    = utils::rad2deg(th);



                        driftDetailsMsg.cumulative_traveled_distance = utils::getMeter(&receivedMessage[37]);
                        driftDetailsMsg.cumulative_traveled_rotation = utils::getRad32(&receivedMessage[41]);
                        driftDetailsMsg.error_percentage            = ((uint64_t) utils::getUInt32(&receivedMessage[45]))/1000.0;
                        driftDetailsMsg.cluster_id                  = utils::getUInt16(&receivedMessage[49]);
                        driftDetailsMsg.drift_correction_type        = receivedMessage[51];
                        driftDetailsMsg.acq_quality_estimate         = receivedMessage[52];
                    
                        pubDriftDetails_.publish(driftDetailsMsg);    
                    }
                    else
                    {
                        ROS_INFO("Drift correction details message shows mismatch between Jupiter and AccerionDriverROS versions!");
                    }
                    
                    break;
                }
                case Commands::STR_LINE_FOLLOWER:
                {
                    geometry_msgs::PolygonStamped lineFollowerArrowPolygon;

                    lineFollowerArrowPolygon.header.frame_id = absRefFrame_;
                    lineFollowerArrowPolygon.header.stamp = rosTimeStamp;

                    lineFollowerArrowPolygon.polygon.points.clear();

                    geometry_msgs::Point32 sensorPoint, closestPointOnLine;
                    
                    sensorPoint.x = utils::getMeter(&receivedMessage[13]);
                    sensorPoint.y = utils::getMeter(&receivedMessage[17]);
                    sensorPoint.z = 0.0;  

                    closestPointOnLine.x = utils::getMeter(&receivedMessage[25]);
                    closestPointOnLine.y = utils::getMeter(&receivedMessage[29]);
                    closestPointOnLine.z = 0.0;

                    lineFollowerArrowPolygon.polygon.points.push_back(sensorPoint);
                    lineFollowerArrowPolygon.polygon.points.push_back(closestPointOnLine);

                    pubLineFollower_.publish(lineFollowerArrowPolygon);

                    break;
                }
                case Commands::STR_SIGNATURE_MARKER:  //map messages
                {                   
                    int nMarkerInMessage = utils::getUInt16(&receivedMessage[5]);
                    int messageHeaderSize = 7;
                    int markerMessageSize = 23;

                    int prevMarkerCount = mapPointCloud_.width;
                    mapPointCloud_.width += nMarkerInMessage;

                    pc2Modifier_->resize(mapPointCloud_.width);

                    sensor_msgs::PointCloud2Iterator<float> iterX(mapPointCloud_, "x");
                    sensor_msgs::PointCloud2Iterator<float> iterY(mapPointCloud_, "y");
                    sensor_msgs::PointCloud2Iterator<float> iterZ(mapPointCloud_, "z");
                    sensor_msgs::PointCloud2Iterator<uint16_t> iterClusterID(mapPointCloud_, "clusterID");
                    sensor_msgs::PointCloud2Iterator<uint32_t> iterMarkerID(mapPointCloud_, "markerID");
                    sensor_msgs::PointCloud2Iterator<uint8_t> iterMarkerStatus(mapPointCloud_, "markerStatus");

                    iterX += prevMarkerCount;
                    iterY += prevMarkerCount;
                    iterZ += prevMarkerCount;
                    iterClusterID += prevMarkerCount;
                    iterMarkerID += prevMarkerCount;
                    iterMarkerStatus += prevMarkerCount;

                    for(unsigned int ii = 0; ii < nMarkerInMessage; ii++, ++iterX, ++iterY, ++iterZ, ++iterClusterID, ++iterMarkerID, ++iterMarkerStatus)
                    {
                        float markerX = utils::getMeter(&receivedMessage[messageHeaderSize + ii * markerMessageSize]);
                        float markerY = utils::getMeter(&receivedMessage[messageHeaderSize + ii * markerMessageSize + 4]);
                        float markerZ = 0.0f;
                        uint16_t clusterID = utils::getUInt16(&receivedMessage[messageHeaderSize + ii * markerMessageSize + 8]);
                        uint32_t markerID = utils::getUInt32(&receivedMessage[messageHeaderSize + ii * markerMessageSize + 10]);
                        uint8_t markerStatus = receivedMessage[messageHeaderSize + ii * markerMessageSize + 14];

                        // std::cout   << "marker " << std::setw(5) << ii << ": "
                        //             << " x: " << std::setw(10) << std::setprecision(3) << markerX 
                        //             << " y: " << std::setw(10) << std::setprecision(3) << markerY 
                        //             << " clID: " << std::setw(10) << std::setprecision(0) << clusterID 
                        //             << " mID: " << std::setw(10) << std::setprecision(0) << markerID 
                        //             << " status: " << std::setw(10) << std::setprecision(0) << markerStatus 
                        //             << std::endl;

                        *iterX = markerX;
                        *iterY = markerY;
                        *iterZ = markerZ;
                        *iterClusterID = clusterID;
                        *iterMarkerID = markerID;
                        *iterMarkerStatus = markerStatus;
                    }

                    break;
                }
                case Commands::INT_ARUCO_MARKER: // detected aruco markers' relative poses w.r.t. sensor coord. frame
                {
                    visualization_msgs::MarkerArray arucoMarkerArray;
                    visualization_msgs::Marker arucoMarker, arucoMarkerText, arucoMarkerArrow;

                    // arucoMarker.header.seq = signatureID;
                    arucoMarker.header.frame_id = absSensorFrame_;
                    arucoMarker.header.stamp = rosTimeStamp;
                    arucoMarker.id = (int)utils::getUInt16(&receivedMessage[25]);
                    std::stringstream ss;
                    ss << arucoMarker.id;
                    arucoMarker.ns = ss.str() + "/geometry"; // ss.str();
                    arucoMarker.type = visualization_msgs::Marker::CUBE;
                    arucoMarker.action = visualization_msgs::Marker::ADD;
                    float markerYaw = utils::getRad32(&receivedMessage[21]);
                    arucoMarker.pose.position.x = utils::getMeter(&receivedMessage[13]);
                    arucoMarker.pose.position.y = utils::getMeter(&receivedMessage[17]);
                    arucoMarker.pose.position.z = 0;
                    arucoMarker.pose.orientation.x = 0.0;
                    arucoMarker.pose.orientation.y = 0.0;
                    arucoMarker.pose.orientation.z = sin(markerYaw / 2.0);
                    arucoMarker.pose.orientation.w = cos(markerYaw / 2.0);
                    arucoMarker.frame_locked = false;

                    arucoMarkerText = arucoMarker;
                    arucoMarkerArrow = arucoMarker;

                    arucoMarker.scale.x = 0.02;
                    arucoMarker.scale.y = 0.02;
                    arucoMarker.scale.z = 0.001;
                    arucoMarker.color.a = 0.5;
                    arucoMarker.color.r = 255;
                    arucoMarker.color.g = 255;
                    arucoMarker.color.b = 255;
                    
                    arucoMarkerArray.markers.push_back(arucoMarker);
                    
                    arucoMarkerText.ns = ss.str() + "/text";
                    arucoMarkerText.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
                    arucoMarkerText.text = ss.str();
                    arucoMarkerText.scale.z = 0.004;
                    arucoMarkerText.color.a = 1.0;
                    arucoMarkerText.color.r = 0;
                    arucoMarkerText.color.g = 1.0;
                    arucoMarkerText.color.b = 1.0;
                                       
                    arucoMarkerArray.markers.push_back(arucoMarkerText);

                    arucoMarkerArrow.ns = ss.str() + "/arrow";
                    arucoMarkerArrow.type = visualization_msgs::Marker::ARROW;
                    arucoMarkerArrow.color.a = 0.5;
                    arucoMarkerArrow.scale.x = 0.01;
                    arucoMarkerArrow.scale.y = 0.0015;
                    arucoMarkerArrow.scale.z = arucoMarkerArrow.scale.y;
                    arucoMarkerArrow.color.r = 255;

                    arucoMarkerArray.markers.push_back(arucoMarkerArrow);

                    pubArucoVisualization_.publish(arucoMarkerArray);
                    break;
                }
                case Commands::ACK_NEW_POSITION_IS_SET:  //new position
                {
                    ROS_INFO("ACK: new position is set!");
                    break;
                }
                case Commands::ACK_SIGNATURE_MARKER_MAP_START_STOP:  //start or stop of map messages
                {            
                    if(receivedMessage[5] == Commands::cmdTrue)
                    {
                        ROS_INFO("----------received start of map message!");
                        
                        mapPointCloud_.header.stamp     = rosTimeStamp; 
                        mapPointCloud_.height           = 1;
                        mapPointCloud_.width            = 0; 
                        mapPointCloud_.data.resize(mapPointCloud_.width * mapPointCloud_.point_step);

                    }
                    else if(receivedMessage[5] == Commands::cmdFalse)
                    {
                        ROS_INFO("----------received stop of map message!");
                        ROS_INFO("number of markers in map: %d", mapPointCloud_.width);
                        pubMapAsPC2_.publish(mapPointCloud_);
                    }

                    break;
                }
                case Commands::ACK_CONSOLE_OUTPUT_INFO:
                {
                    uint32_t messageSize = utils::getUInt32(&receivedMessage[5]);

                    std::string consoleOutputMsg(&receivedMessage[9], &receivedMessage[9+messageSize]);

                    ROS_INFO("ACK_CONSOLE_OUTPUT_INFO: %s", consoleOutputMsg.c_str());
                    break;
                }
                case Commands::ACK_CLUSTER_REMOVED:
                {
                    ROS_INFO("ACK_CLUSTER_REMOVED");
                    break;
                }
                case Commands::ACK_LINE_FOLLOWER_MODE:
                {
                    ROS_INFO("ACK_LINE_FOLLOWER_MODE");
                    break;
                }
                case Commands::ACK_RECOVERY_MODE:
                {
                    ROS_INFO("ACK_RECOVERY_MODE");
                    break;
                }
                case Commands::ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED:
                {
                    ROS_INFO("ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED");
                    break;
                }
                case Commands::ACK_IDLE_MODE:
                {
                    ROS_INFO("ACK_IDLE_MODE");
                    break;
                }
                case Commands::ACK_LOCALIZATION_MODE:
                {
                    ROS_INFO("ACK_LOCALIZATION_MODE");
                    break;
                }
                case Commands::ACK_MAPPING_MODE:
                {
                    ROS_INFO("ACK_MAPPING_MODE");
                    break;
                }
                case Commands::ACK_SOFTWARE_VERSION:
                {
                    ROS_INFO("ACK_SOFTWARE_VERSION");
                    break;
                }
                case Commands::ACK_SOFTWARE_HASH:
                {
                    ROS_INFO("ACK_SOFTWARE_HASH");
                    break;
                }
                case Commands::ACK_SENSOR_MOUNT_POSE: // acknowledgement on sensor mount pose
                {
                    sensorMountPose_.x  = utils::getMeter(&receivedMessage[5]);
                    sensorMountPose_.y  = utils::getMeter(&receivedMessage[9]);
                    sensorMountPose_.th = utils::getRad32(&receivedMessage[13]);
                    
                    ROS_INFO("Received Commands::ACK_SENSOR_MOUNT_POSE as x: %f y: %f th: %f", sensorMountPose_.x, sensorMountPose_.y, sensorMountPose_.th);
                    
                    geometry_msgs::TransformStamped outputToSensorTrans;
                    
                    outputToSensorTrans.transform.translation.x = sensorMountPose_.x;
                    outputToSensorTrans.transform.translation.y = sensorMountPose_.y;
                    outputToSensorTrans.transform.translation.z = 0;

                    outputToSensorTrans.transform.rotation.x = 0;
                    outputToSensorTrans.transform.rotation.y = 0;
                    outputToSensorTrans.transform.rotation.z = sin(sensorMountPose_.th / 2.0);
                    outputToSensorTrans.transform.rotation.w = cos(sensorMountPose_.th / 2.0);

                    outputToSensorTrans.header.stamp = rosTimeStamp;
                    
                    if(relTransOn_)
                    {
                        outputToSensorTrans.header.frame_id = relBaseFrame_;
                        outputToSensorTrans.child_frame_id  = relSensorFrame_;
                        staticTfBroadcaster_.sendTransform(outputToSensorTrans);
                    }

                    if(absTransOn_)
                    {
                        outputToSensorTrans.header.frame_id = absBaseFrame_;
                        outputToSensorTrans.child_frame_id  = absSensorFrame_;
                        staticTfBroadcaster_.sendTransform(outputToSensorTrans);
                    }
                    break;
                }
                case Commands::ACK_UDP_INFO:
                {
                    char unicastIp[20];
                    inet_ntop(AF_INET, &receivedMessage[7], unicastIp, 20);

                    std::string messageType, udpMode;

                    switch (receivedMessage[5])
                    {
                        case 0x01:
                        {
                            messageType = "inactive";
                            break;
                        }
                        case 0x02:
                        {
                            messageType = "only streaming";
                            break;
                        }
                        case 0x03:
                        {
                            messageType = "only intermittent";
                            break;
                        }
                        case 0x04:
                        {
                            messageType = "streaming & intermittent";
                            break;
                        }
                    }

                    switch (receivedMessage[6])
                    {
                        case 0x01:
                        {
                            udpMode = "broadcast";
                            break;
                        }
                        case 0x02:
                        {
                            udpMode = "unicast";
                            break;
                        }
                    }

                    // ROS_INFO("ACK_UDP_INFO: the sensor is in \n UDP %s mode \n message type %s \n unicast ip address %s ", udpMode.c_str(), messageType.c_str(), unicastIp);
                    break;
                }
            } // switch(receivedCommandID) 
        }
        else //(receivedSerialNumber != jupiterSerial_)
        {
            if(noInfoCount_ % 2000 == 0 && noInfoCount_ > 0)
            {
                ROS_WARN("%s: no data from Jupiter with SN: %d , received data from SN: %d ", nodeNs_.c_str(), jupiterSerial_, receivedSerialNumber);
            }

            // if serial number is not set, remember first found accerion sensor
            if (jupiterSerial_ == 0)
            {
                ROS_INFO("%s: no serial set, using first found accerion sensor with SN: %d", nodeNs_.c_str(), receivedSerialNumber);
                jupiterSerial_ = receivedSerialNumber;
            }

        }
    }

    if(infoCount_ == 0)
    {
        noInfoCount_++;
        
        if(noInfoCount_ > 1000)
        {
            sensorConnected_ = false;
        }
    }
 
    // std::cout << "infoCount_: " << infoCount_  << "  noInfoCount_: " << noInfoCount_ << std::endl;
    
    if(noInfoCount_ % 100 == 0 && noInfoCount_ > 0)
    {
        ROS_WARN("%s: no sensor available with serial number %d", nodeNs_.c_str(), jupiterSerial_);
    } 

    return 0;
}

int AccerionDriverROS::shutdown()
{
    delete udpManager_;
}

void AccerionDriverROS::callbackResetPose(const geometry_msgs::PoseStamped::ConstPtr& resetPose)
{   
    std::vector<uint8_t> dataToTransmit;

    ROS_INFO("new input reset pose: %f , %f , %f", resetPose->pose.position.x, resetPose->pose.position.y, getThetaFromQuat(resetPose->pose.orientation)*180.0/M_PI);

    int32_t posX  = resetPose->pose.position.x*1e6;
    int32_t posY  = resetPose->pose.position.y*1e6;
    int32_t theta = getThetaFromQuat(resetPose->pose.orientation)*100.0*180.0/M_PI; 

    uint32_t uposX  = resetPose->pose.position.x*1e6;
    uint32_t uposY  = resetPose->pose.position.y*1e6;
    uint32_t utheta = getThetaFromQuat(resetPose->pose.orientation)*100.0*180.0/M_PI; 

    uint8_t serUint32[4];

    udpManager_->serializeUInt32(uposX, serUint32);
    dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
    udpManager_->serializeUInt32(uposY,serUint32);
    dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
    udpManager_->serializeUInt32(utheta, serUint32);
    dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);

    udpManager_->sendUDPMessageToJupiter(Commands::CMD_SET_NEW_POSE, dataToTransmit);
}

void AccerionDriverROS::callbackExternalReferencePose(const nav_msgs::Odometry::ConstPtr& ext_ref_msg)
{
    if(sensorConnected_)
    {   
        uint64_t timeDiffUSec = 0;

        if(extRefPoseLatency_ == 0.0)
        {
            ros::Time timeNow = ros::Time::now();
            ros::Duration timeDifference;

            if(timeNow < ext_ref_msg->header.stamp)
            {
                ROS_WARN("Timestamp of the incoming ext reference pose message is in the future! Ignoring it and putting time offset to 0");
                timeDifference = ros::Duration(0.0);
            }
            else
            {
                timeDifference = timeNow - ext_ref_msg->header.stamp;
                if(timeDifference.toSec() > 1.0)
                {
                    ROS_WARN("Time offset of the incoming ext reference pose message exceeds 1 second!");
                }
            }

            timeDiffUSec = (uint64_t)(timeDifference.toSec()*1e6);

            // ROS_INFO("new input ext reference pose: %f [m], %f [m], %f [deg] with a time offset %.0f us", 
            //             ext_ref_msg->pose.pose.position.x, 
            //             ext_ref_msg->pose.pose.position.y, 
            //             getThetaFromQuat(ext_ref_msg->pose.pose.orientation)*180.0/M_PI, 
            //             (double)timeDiffUSec);
        }   
        else
        {
            timeDiffUSec = extRefPoseLatency_*1e3;
        }

        // ROS_INFO("Inside callbackExternalReferencePose!");
        udpManager_->sendExternalReferenceMessage(Commands::CMD_SET_POSE_AND_COVARIANCE,
                            timeDiffUSec, 
                            ext_ref_msg->pose.pose.position.x, 
                            ext_ref_msg->pose.pose.position.y, 
                            getThetaFromQuat(ext_ref_msg->pose.pose.orientation), 
                            std::sqrt(ext_ref_msg->pose.covariance[0]), 
                            std::sqrt(ext_ref_msg->pose.covariance[7]), 
                            std::sqrt(ext_ref_msg->pose.covariance[35]));
    }
}

double AccerionDriverROS::getThetaFromQuat(const geometry_msgs::Quaternion quatIn)
{
    tf::Quaternion q(quatIn.x, quatIn.y, quatIn.z, quatIn.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    if(yaw<-M_PI) yaw += 2.0*M_PI;
    if(yaw>M_PI) yaw += -2.0*M_PI;
    
    return yaw;
}

bool AccerionDriverROS::callbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request  &req,
                                              accerion_driver_msgs::Cluster::Response &res)
{
    std::vector<uint8_t> dataToTransmit;
    uint8_t serUint16[2];
    udpManager_->serializeUInt16(req.cluster_id, serUint16);
    dataToTransmit.insert(dataToTransmit.end(), serUint16, serUint16+2);
    udpManager_->sendUDPMessageToJupiter(Commands::CMD_DELETE_CLUSTER, dataToTransmit);

    // TODO: implement acknowledgement with request based comm with the sensor
    res.success = true;
    res.message = "Deleting cluster " + std::to_string(req.cluster_id) + ". Note: no acknowledgement check is done. TODO";

    return true;
}

bool AccerionDriverROS::callbackServiceDeleteAllClusters(std_srvs::SetBool::Request  &req,
                                                  std_srvs::SetBool::Response &res)
{
    if(req.data == true)
    {
        std::vector<uint8_t> dataToTransmit;
        udpManager_->sendUDPMessageToJupiter(Commands::CMD_REMOVE_COMPLETE_CLUSTER_LIBRARY, dataToTransmit);
        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Deleting ALL clusters. Note: no acknowledgement check is done. TODO";
    }
    else
    {
        res.success = true;
        res.message = "usage: rosservice call " + serviceServerDeleteAllClusters_.getService() + " true";
    }

    return true;
}

bool AccerionDriverROS::callbackServiceRecoveryMode(accerion_driver_msgs::SetRecoveryMode::Request  &req,
                                                   accerion_driver_msgs::SetRecoveryMode::Response &res)
{
    std::vector<uint8_t> dataToTransmit = {req.recovery_mode ? Commands::cmdTrue : Commands::cmdFalse};
    uint8_t searchRad = req.search_radius;
    dataToTransmit.push_back(searchRad);
    udpManager_->sendUDPMessageToJupiter(Commands::CMD_SET_RECOVERY_MODE, dataToTransmit);

    // TODO: implement acknowledgement with request based comm with the sensor
    res.success = true;
    res.msg = "Setting recovery mode to " + std::to_string(req.recovery_mode) + " and search radius to " + std::to_string(req.search_radius) + " Note: no acknowledgement check is done. TODO";

    return true;

}

bool AccerionDriverROS::callbackServiceMode(accerion_driver_msgs::ModeCommand::Request  &req,
                                            accerion_driver_msgs::ModeCommand::Response &res)
{
    uint8_t outgoingCommandID;

    if(req.mode.compare("localization") == 0)
    {
        outgoingCommandID = Commands::CMD_SET_DRIFT_CORRECTION_MODE;
    }
    else if(req.mode.compare("idle") == 0)
    {
        outgoingCommandID = Commands::CMD_SET_IDLE_MODE;
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerMode_.getService() + " localization/idle start/stop";
        return true;
    }

    if(req.command.compare("start") != 0 && req.command.compare("stop") != 0)
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerMode_.getService() + " localization/idle start/stop";
        return true;
    }
    else
    {
        udpManager_->sendUDPMessageToJupiter(outgoingCommandID, 
                                        {req.command.compare("start") == 0 ? Commands::cmdTrue : Commands::cmdFalse});

        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Setting " + req.mode + " mode to " + req.command + ". Note: no acknowledgement check is done. TODO";
    }
    return true;
}

bool AccerionDriverROS::callbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request  &req,
                                                       accerion_driver_msgs::ModeClusterCommand::Response &res)
{
    uint8_t outgoingCommandID;

    if(req.mode.compare("mapping") == 0)
    {
        outgoingCommandID = Commands::CMD_START_MARKERLESS_LEARNING;
    }
    else if(req.mode.compare("line_following") == 0)
    {
        outgoingCommandID = Commands::CMD_SET_LINE_FOLLOWER;
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerModeWithCluster_.getService() + " mapping/line_following 42 start/stop";
        return true;
    }

    if(req.command.compare("start") != 0 && req.command.compare("stop") != 0)
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerModeWithCluster_.getService() + " mapping/line_following 42 start/stop";
        return true;
    }
    else
    {
        uint8_t boolCmd = req.command.compare("start") == 0 ? Commands::cmdTrue : Commands::cmdFalse; 

        std::vector<uint8_t> dataToTransmit = {boolCmd};
        uint8_t serUint16[2];
        udpManager_->serializeUInt16(req.cluster_id, serUint16);
        dataToTransmit.insert(dataToTransmit.end(), serUint16, serUint16+2);
        udpManager_->sendUDPMessageToJupiter(outgoingCommandID, dataToTransmit);

        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Setting " + req.mode + " mode to " + req.command + " on cluster ID:" + std::to_string(req.cluster_id) + ". Note: no acknowledgement check is done. TODO";
    }
    return true;
}

bool AccerionDriverROS::callbackServiceRequest(accerion_driver_msgs::RequestCommand::Request  &req,
                                               accerion_driver_msgs::RequestCommand::Response &res)
{
    uint8_t outgoingCommandID;

    if(req.request_command.compare("map") == 0)
    {
        outgoingCommandID = Commands::CMD_GET_SIGNATURE_MARKER_MAP;
    }
    else if(req.request_command.compare("sensor_mount_pose") == 0)
    {
        outgoingCommandID = Commands::CMD_GET_SENSOR_MOUNT_POSE;
    }
    else if(req.request_command.compare("version") == 0)
    {
        outgoingCommandID = Commands::CMD_GET_SOFTWARE_VERSION;
    }
    else if(req.request_command.compare("version_hash") == 0)
    {
        outgoingCommandID = Commands::CMD_GET_SOFTWARE_HASH;
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerRequest_.getService() + " map/sensor_mount_pose/version/version_hash";
        return true;
    }

    std::vector<uint8_t> dataToTransmit;
    udpManager_->sendUDPMessageToJupiter(outgoingCommandID, dataToTransmit);
    // TODO: implement acknowledgement with request based comm with the sensor
    res.success = true;
    res.message = "Requsting " + req.request_command + ". Note: no acknowledgement check is done. TODO";

    return true;
}


//ONLY USE FOR TESTING
bool AccerionDriverROS::callbackServiceResetPoseByTfLookup(accerion_driver_msgs::ParentChildFrames::Request  &req,
                                                           accerion_driver_msgs::ParentChildFrames::Response &res)
{   
    tf::StampedTransform currTf;

    try
    {
        double resetPosX  = 0.0;
        double resetPosY  = 0.0;
        double resetTheta = 0.0;

        ROS_INFO("looking up tf from %s to %s", req.parent.c_str(), req.child.c_str());
        tfListener_.lookupTransform(req.parent, req.child, ros::Time(0), currTf);

        ros::Time timeNow = ros::Time::now();
        // ROS_INFO("current time %ds %dns found tf at %ds %dns", timeNow.sec, timeNow.nsec, currTf.stamp_.sec, currTf.stamp_.nsec);
        ros::Duration timeDifference;

        if(timeNow < currTf.stamp_)
        {
            ROS_WARN("Timestamp of the looked up tf is in the future! Ignoring it and putting time offset to 0");
            timeDifference = ros::Duration(0.0);
        }
        else
        {
            timeDifference = timeNow - currTf.stamp_;
            if(timeDifference.toSec() > 1.0)
            {
                ROS_WARN("Time offset of the looked up tf exceeds 1 second!");
            }
        }

        uint64_t timeDiffUSec = (uint64_t)(timeDifference.toSec()*1e6);

        tf::Quaternion tfQuat;
        tfQuat = currTf.getRotation();

        tf::Matrix3x3 m(tfQuat);
        double roll, pitch;
        m.getRPY(roll, pitch, resetTheta);

        tf::Vector3 tfVec;

        tfVec = currTf.getOrigin();

        resetPosX = tfVec.getX();
        resetPosY = tfVec.getY();

        ROS_INFO("new input reset pose: %f [m], %f [m], %f [deg] with a time offset %.0f us", resetPosX, resetPosY, resetTheta*180.0/M_PI, (double)timeDiffUSec);

        udpManager_->sendExternalReferenceMessage(Commands::CMD_SET_POSE_AND_COVARIANCE, 
                            timeDiffUSec,
                            resetPosX, 
                            resetPosY, 
                            resetTheta, 
                            0, 
                            0, 
                            0);

        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Resetting pose by tf lookup from parent frame " + req.parent + " to child frame " + req.child + ". Note: no acknowledgement check is done. TODO";
    }
    catch(...)
    {
        ROS_INFO("%s: lookupTransform error! Does the tf from %s to %s exist?", nodeNs_.c_str(), req.parent.c_str(), req.child.c_str());
        res.success = true;
        res.message = "No tf could be found from parent frame " + req.parent + " to child frame " + req.child;
    }
    return true;
}

bool AccerionDriverROS::callbackServiceSetPose(accerion_driver_msgs::Pose::Request  &req,
                                               accerion_driver_msgs::Pose::Response &res)
{
    uint8_t outgoingCommandID;
    std::vector<uint8_t> dataToTransmit;

    if(req.selection.compare("sensor_mount") == 0)
    {
        outgoingCommandID = Commands::CMD_SET_SENSOR_MOUNT_POSE;

        ROS_INFO("new input sensor_mount pose: %f , %f , %f", req.x, req.y, req.theta*180.0/M_PI);

        uint32_t uposX  = req.x*1e6;
        uint32_t uposY  = req.y*1e6;
        uint32_t utheta = req.theta*100.0; 

        uint8_t serUint32[4];

        udpManager_->serializeUInt32(uposX, serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
        udpManager_->serializeUInt32(uposY,serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
        udpManager_->serializeUInt32(utheta, serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);

    }
    else if(req.selection.compare("sensor_output") == 0)
    {
        outgoingCommandID = Commands::CMD_SET_NEW_POSE;

        ROS_INFO("new input sensor_output pose: %f , %f , %f", req.x, req.y, req.theta*180.0/M_PI);

        uint32_t uposX  = req.x*1e6;
        uint32_t uposY  = req.y*1e6;
        uint32_t utheta = req.theta*100.0; 

        uint8_t serUint32[4];

        udpManager_->serializeUInt32(uposX, serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
        udpManager_->serializeUInt32(uposY,serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
        udpManager_->serializeUInt32(utheta, serUint32);
        dataToTransmit.insert(dataToTransmit.end(), serUint32, serUint32+4);
    }
    else
    {
        res.success = false;
        res.message = "units are x[m], y[m], theta[deg]. usage: rosservice call " + serviceServerSetPose_.getService() + " sensor_mount/sensor_output 0 0 0";
        return true;
    }

    udpManager_->sendUDPMessageToJupiter(outgoingCommandID, dataToTransmit);
    // TODO: implement acknowledgement with request based comm with the sensor
    res.success = true;
    res.message = "Setting " + req.selection + " pose to " + std::to_string(req.x) + "[m]," + std::to_string(req.y) + "[m]," + std::to_string(req.theta) + "[deg]" + " . Note: no acknowledgement check is done. TODO";

    return true;
}


bool AccerionDriverROS::callbackServiceSetUDPSettings(accerion_driver_msgs::UDPSettings::Request  &req,
                                                      accerion_driver_msgs::UDPSettings::Response &res)
{
    uint8_t outgoingCommandID;
    std::vector<uint8_t> dataToTransmit;

    outgoingCommandID = Commands::CMD_SET_UDP_SETTINGS;

    uint8_t ipSer[4];
    inet_pton(AF_INET, req.unicast_ip_address.c_str(), &ipSer);
    dataToTransmit.insert(dataToTransmit.end(), ipSer, ipSer+4);

    if((req.message_type > 0 || req.message_type < 5) && (req.udp_mode == 1 || req.udp_mode == 2))
    {
        dataToTransmit.push_back(req.message_type);
        dataToTransmit.push_back(req.udp_mode);
        udpManager_->sendUDPMessageToJupiter(outgoingCommandID, dataToTransmit);
        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Setting UDP settings to: unicastIp: " + req.unicast_ip_address + ", message type: " + std::to_string(req.message_type) + ", udp mode: " + std::to_string(req.udp_mode) + " . Note: no acknowledgement check is done. TODO";
    }
    else
    {
        res.success = false;
        res.message = "message type can be [1,2,3,4] and udp mode can be [1,2] only.";
    }
    
    // ROS_INFO("ACK_UDP_INFO: the sensor is in \n UDP %s mode \n message type %s \n unicast ip address %s ", std::to_string(req.udp_mode),std::to_string(req.message_type), req.unicast_ip_address);
                    
    return true;
}