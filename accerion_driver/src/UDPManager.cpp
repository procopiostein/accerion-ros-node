/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "UDPManager.h"

UDPManager::UDPManager(int jupiterSerial)
{
    debugMode_          = false;
    debugModeStreaming_ = false;

    udpReceiver_    = new UDPReceiver(REMOTE_RECEIVE_PORT);
    udpTransmitter_ = new UDPTransmitter(JUPITER_RECEIVE_PORT);

    serialNumber_ = jupiterSerial;

    crc8_.crcInit();
}

UDPManager::~UDPManager()
{

}

bool UDPManager::receiveMessage()
{
    if(udpReceiver_->ReceiveMessage())
    {
        receivedIpAddressStr_  = udpReceiver_->ipAddressStr_;
        receivedMessage_       = udpReceiver_->getReceivedMessage();
        receivedNumOfBytes_    = udpReceiver_->getReceivedNumOfBytes();
        receivedCommand_.clear();
        if(debugMode_)
            std::cout << "From UDP Manager, received msg: " << +receivedMessage_ << " and received number of bytes is := " << receivedNumOfBytes_ << std::endl;

        //udpTransmitter_->transmitMessage(receivedMessage_, receivedNumOfBytes_);

        uint8_t         receivedSerialNumberData[4];

        for(unsigned int i=0; i<receivedNumOfBytes_; i++)
        {
            /*Get Serial Number*/
            if(i < 4)
            {
                receivedSerialNumberData[i] = receivedMessage_[i];
            }
            /*Get Command Number*/
            else if(i == 4)
            {
                receivedCommandID_ = receivedMessage_[i];
            }   
            /*Get Command Data*/
            else if(i > 4 && i < receivedNumOfBytes_ - 1)
            {
                receivedCommand_.push_back(receivedMessage_[i]);
            }
            /*Get CRC8 Data*/
            else
                receivedCRC8_ = receivedMessage_[i];
        }
        /*Check serial number*/
        receivedSerialNumber_   = ntohl(*((uint32_t*)&receivedSerialNumberData));
        uint8_t calculatedCRC8 = crc8_.crcFast(receivedMessage_, receivedNumOfBytes_ - 1);
        
        if(receivedCRC8_ != calculatedCRC8)
        {
            if(debugMode_)
                std::cout << "Received UDP Message does not have the correct crc8 code, it has the wrong code := " << std::hex << receivedCRC8_ << std::dec << std::endl;
        }

        if(debugMode_)
        {
            std::cout << "Received serialNumber is := "      << std::hex << receivedSerialNumber_ << std::dec << std::endl;
            std::cout << "Received command number is := " << std::hex << +receivedCommandID_ << std::dec << std::endl;
            // for (int x = 0; x != example.size(); ++x)
            // {
            //  std::cout << receivedCommand_[x] << "- subscripting" << std::endl;
            //  std::cout << example.at(x) << "- calling at member" << std::endl;
            // }
            std::cout << "Received command data is := ";
            for(unsigned int i=0; i < receivedCommand_.size(); ++i)
                std::cout << +receivedCommand_[i];
            std::cout << std::endl;
        }
        return true;
    }   
    else
    {
        return false;
    }
}

bool UDPManager::formAndSendMessage()
{
    bool success;

    transmittedSerialNumber_ = serialNumber_; //htonl(serialNumber_);
    
    serializeUInt32(transmittedSerialNumber_, transmittedSerialNumberData_, false);

    transmittedMessage_.insert(transmittedMessage_.end(), transmittedSerialNumberData_, transmittedSerialNumberData_ + 4);
    transmittedMessage_.push_back(transmittedCommandID_);
    transmittedMessage_.insert( transmittedMessage_.end(), transmittedData_.begin(), transmittedData_.end());
    transmittedCRC8_    = crc8_.crcFast((uint8_t*) transmittedMessage_.data(), transmittedMessage_.size());
    transmittedMessage_ .push_back(transmittedCRC8_);
    transmittedNumOfBytes_  = transmittedMessage_.size();

    success = udpTransmitter_->transmitMessage(transmittedMessage_.data(), transmittedNumOfBytes_);
    // success = udpTransmitter_->transmitMessage(reinterpret_cast<char*>(transmittedMessage_.data()), transmittedNumOfBytes_);

    if(debugModeStreaming_)
    {
        std::cout << "From UDP Manager, Transmitted serialNumber is := "; //    << std::hex << +transmittedSerialNumberData_ << std::dec << std::endl;
        for(unsigned int i=0; i < 4; ++i)
            std::cout << std::hex << +transmittedSerialNumberData_[i];
        std::cout << std::dec << std::endl;

        std::cout << "From UDP Manager, Transmitted serialNumber is := "    << serialNumber_ << std::endl;
        std::cout << "From UDP Manager, Transmitted command number is := " << std::hex << +transmittedCommandID_ << std::dec << std::endl;
        
        std::cout << "From UDP Manager, Transmitted command data is := ";
        for(unsigned int i=0; i < transmittedData_.size(); ++i)
            std::cout << std::hex << +transmittedData_[i];
        std::cout << std::dec << std::endl;
    
        std::cout << "From UDP Manager, transmitted msg: ";
        for(unsigned int i=0; i < transmittedMessage_.size(); ++i)
            std::cout << std::hex << +transmittedMessage_[i];
        std::cout << std::dec << std::endl;

        std::cout << "From UDP Manager, transmitted number of bytes is := " << transmittedNumOfBytes_ << std::endl;
    }
    transmittedMessage_.clear();
    transmittedData_.clear();

    return success;
}


bool UDPManager::sendUDPMessageToJupiter(const uint8_t commandID, const std::vector<uint8_t>& data)
{
    bool success;

    transmittedCommandID_ = commandID;
    transmittedData_      = data;

    success = formAndSendMessage();

    return success;
}

bool UDPManager::sendRawDataMessage(uint8_t commandID, const uint8_t *rawData, unsigned int length)
{
    if(debugMode_)
    {
        std::cout << "From sendRawDataMessage, Command ID := "                  << std::hex << commandID << std::dec << std::endl; 
        std::cout << "From sendRawDataMessage, length of raw data is := "       << length << std::endl; 
    }

    transmittedCommandID_   = commandID;

    for(unsigned int i=0; i<length; i++)
    {
        transmittedData_.push_back(rawData[i]);
    }

    return formAndSendMessage();
}

bool UDPManager::sendDataMessage(uint8_t commandID, const std::vector<uint8_t> data)
{
    if(debugMode_)
    {
        std::cout << "From sendDataMessage, Command ID := "                 << std::hex << commandID << std::dec << std::endl; 
        std::cout << "From sendDataMessage, length of raw data is := "      << data.size() << std::endl; 
    }

    transmittedCommandID_   = commandID;
    transmittedData_        = data;

    return formAndSendMessage();
}


bool UDPManager::sendBooleanAcknowledgeMessage(uint8_t commandID, bool ackValue)
{
    transmittedCommandID_   = commandID;
    transmittedData_.push_back((ackValue == true)? 0x01 : 0x02);

    return formAndSendMessage();
}

bool UDPManager::sendBasicAcknowledgeMessage(uint8_t commandID)
{
    if(debugMode_)
        std::cout << "From sendBasicAcknowledgeMessage, Command ID := " << std::hex << commandID << std::dec << std::endl; 

    transmittedCommandID_   = commandID;

    return formAndSendMessage();
}

bool UDPManager::sendExternalReferenceMessage(   uint8_t commandID, const uint64_t timeOffsetInUSecs,
                                        const float &absPoseX, const float &absPoseY, const float &absPoseTheta, 
                                        const float &stdDevPoseX, const float &stdDevPoseY, const float &stdDevPoseTheta)
{
    if(debugModeStreaming_)
    {
        printf("sendExternalReferenceMessage udp: cmdID = 0x%2x := %2d",  commandID, (unsigned)commandID);
        std::cout << "From sendExternalReferenceMessage, Command ID := "         << std::hex                 << commandID       << std::dec << std::endl; 
        std::cout << "From sendExternalReferenceMessage, AbsPosX := "            << std::setprecision(6)     << absPoseX        << std::endl; 
        std::cout << "From sendExternalReferenceMessage, AbsPosY := "            << std::setprecision(6)     << absPoseY        << std::endl; 
        std::cout << "From sendExternalReferenceMessage, AbsAngle := "           << std::setprecision(6)     << absPoseTheta    << std::endl; 
        std::cout << "From sendExternalReferenceMessage, stdDevPoseX := "        << std::setprecision(6)     << stdDevPoseX     << std::endl; 
        std::cout << "From sendExternalReferenceMessage, stdDevPoseY := "        << std::setprecision(6)     << stdDevPoseY     << std::endl; 
        std::cout << "From sendExternalReferenceMessage, stdDevPoseAngle := "    << std::setprecision(6)     << stdDevPoseTheta << std::endl;
    }

    transmittedCommandID_   = commandID;

    int32_t absPosXInt       = absPoseX*1000000.0;
    int32_t absPosYInt       = absPoseY*1000000.0;
    int32_t absThetaInt      = (absPoseTheta*180.0/M_PI)*100.0;
    uint32_t poseStdDevXInt  = stdDevPoseX      *1000000.0;
    uint32_t poseStdDevYInt  = stdDevPoseY      *1000000.0;
    uint32_t poseStdDevThInt = (stdDevPoseTheta*180.0/M_PI) *100.0;

    if(debugModeStreaming_)
    {
        std::cout << "From sendCorrectedOdometryMessage, AbsPosXInt := "            << absPosXInt   << std::endl;
        std::cout << "From sendCorrectedOdometryMessage, AbsPosYInt := "            << absPosYInt   << std::endl;
        std::cout << "From sendCorrectedOdometryMessage, AbsAngleInt := "           << absThetaInt  << std::endl;
        std::cout << "From sendCorrectedOdometryMessage, poseStdDevXInt := "        << poseStdDevXInt   << std::endl; 
        std::cout << "From sendCorrectedOdometryMessage, poseStdDevYInt := "        << poseStdDevYInt   << std::endl; 
        std::cout << "From sendCorrectedOdometryMessage, poseStdDevThInt := "       << poseStdDevThInt  << std::endl;
    }

    uint8_t byteArray[8];

    /*Put in timestamp (TODO: this is a dummy value now)*/
    serializeUInt64(timeOffsetInUSecs, byteArray, false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 8);

    /*Put abs x pos inside*/
    serializeUInt32(absPosXInt, byteArray, false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    /*Put abs y pos inside*/
    serializeUInt32(absPosYInt, byteArray, false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    /*Put abs angle inside*/
    serializeUInt32(absThetaInt, byteArray,  false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    /*Put x std deviation inside*/
    serializeUInt32(poseStdDevXInt, byteArray,  false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    /*Put y std deviation inside*/
    serializeUInt32(poseStdDevYInt, byteArray,  false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    /*Put angle std deviation inside*/
    serializeUInt32(poseStdDevThInt, byteArray,  false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 4);

    return formAndSendMessage();
}

void UDPManager::setReceiver(const std::string ip)
{
    udpTransmitter_->setReceiver(ip);
}

void UDPManager::setSerialNumber(const int serialNumber)
{
    serialNumber_ = serialNumber;
}

bool UDPManager::setJupiterTimeAndDate()
{
    std::time_t now = std::time(0);
 
    std::tm* localTimeNow = std::localtime(&now);

    char timeAndDateMessage [13];

    transmittedCommandID_ = Commands::CMD_SET_TIME_DATE;// 0x83;
    setCharFrom16(&timeAndDateMessage[5], localTimeNow->tm_year + 1900);

    uint8_t byteArray[2];

    serializeUInt16(localTimeNow->tm_year + 1900, byteArray,  false);
    transmittedData_.insert(transmittedData_.end(), byteArray, byteArray + 2);
    transmittedData_.push_back((uint8_t)(localTimeNow->tm_mon + 1));
    transmittedData_.push_back((uint8_t)(localTimeNow->tm_mday));
    transmittedData_.push_back((uint8_t)(localTimeNow->tm_hour));
    transmittedData_.push_back((uint8_t)(localTimeNow->tm_min));
    transmittedData_.push_back((uint8_t)(localTimeNow->tm_sec));

    // sendDataMessage(transmittedCommandID_, transmittedData_);
    return formAndSendMessage();
}



/*Serializes a 16-bit integer into big-endian byte array*/
void UDPManager::serializeUInt16(uint16_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8)  & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 8 ) & 0xFF);
        out[1] = (in & 0xFF);
    }
}


/*Serializes a 32-bit integer into big-endian byte array*/
void UDPManager::serializeUInt32(uint32_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8)  & 0xFF);
        out[2] = ((in >> 16) & 0xFF);
        out[3] = ((in >> 24) & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 24) & 0xFF);
        out[1] = ((in >> 16) & 0xFF);
        out[2] = ((in >> 8 ) & 0xFF);
        out[3] = (in & 0xFF);
    }
}

/*Serializes a 64-bit integer into big-endian byte array*/
void UDPManager::serializeUInt64(uint64_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8 ) & 0xFF);
        out[2] = ((in >> 16) & 0xFF);
        out[3] = ((in >> 24) & 0xFF);
        out[4] = ((in >> 32) & 0xFF);
        out[5] = ((in >> 40) & 0xFF);
        out[6] = ((in >> 48) & 0xFF);
        out[7] = ((in >> 56) & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 56) & 0xFF);
        out[1] = ((in >> 48) & 0xFF);
        out[2] = ((in >> 40) & 0xFF);
        out[3] = ((in >> 32) & 0xFF);
        out[4] = ((in >> 24) & 0xFF);
        out[5] = ((in >> 16) & 0xFF);
        out[6] = ((in >> 8 ) & 0xFF);
        out[7] = (in & 0xFF);
    }
}

void UDPManager::setCharFrom64(char *result, uint64_t number)
{   
    if(IS_BIG_ENDIAN)
    {
        number = htobe64(number);
    }
    else 
    {
        number = htole64(number);
    }
    result = ((char*)number);
    
}

void UDPManager::setCharFrom32(char *result, uint32_t number)
{
    number = htonl(number);
    memcpy(result, &number, 4);
}

void UDPManager::setCharFrom16(char *result, uint16_t number)
{
    number = htons(number);
    memcpy(result, &number, 2);
}

std::string UDPManager::getUDPBinaryMessageInString()
{
    std::stringstream messageInBinary;

    for(unsigned int msgIdx = 0; msgIdx < receivedNumOfBytes_; msgIdx++)
    {
        std::bitset<8> byteInBin((uint8_t)receivedMessage_[msgIdx]); 
        std::string byteInBinStr = byteInBin.to_string();

        messageInBinary << byteInBinStr << " ";
        // messageInBinary << std::setw(3) << (unsigned)((uint8_t)receivedMessage_[msgIdx]) << " ";
        if((msgIdx+1)%4 == 0)
        {
            messageInBinary << std::endl;
        }
    }

    return messageInBinary.str();
}